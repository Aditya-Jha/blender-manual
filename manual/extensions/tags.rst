.. index:: Tags

.. Mark as "orphan" until extensions is out of beta.

:orphan:

.. Keep index link until this page is made public,
   so it's possible to navigate to other extensions pages.

:ref:`Extensions Index <extensions-index>`

***************
Extensions Tags
***************

A different set of tags is available for the different extensions types.
This is the list of the tags currently supported:

Add-ons
=======

* 3D View
* Add Mesh
* Add Curve
* Animation
* Bake
* Compositing
* Development
* Game Engine
* Import-Export
* Lighting
* Material
* Modeling
* Mesh
* Node
* Object
* Paint
* Pipeline
* Physics
* Render
* Rigging
* Scene
* Sculpt
* Sequencer
* System
* Text Editor
* UV
* User Interface

Themes
======

* Dark
* Light
* Print
* Accessibility
* High Contrast
