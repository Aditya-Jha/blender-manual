.. _dope-sheet-shape-key:

****************
Shape Key Editor
****************

This mode shows the :doc:`shape keys </animation/shape_keys/introduction>` of the active object
and lets you create/adjust keyframes for their :ref:`values <bpy.types.ShapeKey.value>`.

.. figure:: /images/editors_dope-sheet_shape-key-editor.png

   The Shape Key Editor.
